<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <nav>
        <div class="menu">
          <div class="logo">
          <img src="img/logo.png" height="48" width="60">
        </div>
          <ul>
            <li><a href="index.html">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div>
      </nav>
      <div class="img"></div>
      <div class="login">
        <?php
            //define variables and set to empty values
            $nameErr = $emailErr = $genderErr =$comment ="";
            $name    = $email    = $gender    = $comment = "";

            if($_SERVER["REQUEST_METHOD"] == "POST"){
                if (empty($_POST["name"])){
                    $nameErr = "Name is required";
                }
                else{
                    $name = test_input($_POST["name"]);
                    //check if name only contains latters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$name)){
                        $nameErr = "Only latters and white space allowed";
                    }
                }
                if (empty($_POST["email"])){
                    $emailErr = "Email is required";
                }
                else{
                    $email = test_input($_POST["email"]);
                    //check if email addres is well - formed
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                        $emailErr = "Invalid email format";
                    }
                }
                if(empty($_POST["comment"])){
                    $comment = "";
                }
                else{
                    $comment = test_input($_POST["comment"]);
                }
                if (empty($_POST["gender"])){
                    $genderErr = "Gender is required";
                }
                else{
                    $gender = test_input($_POST["gender"]);
                }
            }
            function test_input($data){
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data; 
            }
            ?>

            <h2>Regristrasi</h2>
            <p>
                <span class="error">
                    * required field.
                </span>
            </p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            Name: <input type="text" name="name" id="name"value="<?php echo $name;?>">
            <span class="error">
                *<?php echo $nameErr;?>
            </span>
            <br><br>
            E-mail: <input type="text" name="email" id="email"value="<?php echo $email;?>">
            <span class="error">
                *<?php echo $emailErr;?>
            </span>
            <br><br>
            Comment: <textarea name="comment" id="comment" rows="5" cols="40">
                <?php echo $comment;?>
            </textarea>
            <br><br>
            Gender: &nbsp;<input type="radio" name="gender" id="gender" 
            <?php
            if(isset($gender) && $gender == "female") echo"checked";
            ?>value="female">Female &nbsp; &nbsp;<input type="radio" name="gender" 
            <?php
            if(isset($gender) && $gender == "male") echo"checked";
            ?>value="male">Male
            <span class="error">
                *<?php echo $genderErr;?>
            </span>
            <br><br>
            <input type="submit" name="submit" value="Submit">
            </form>

            <?php
            echo "<h2>Your Input: </h2>";
            echo $name;
            echo "<br>";
            echo $email;
            echo "<br>";
            echo $comment;
            echo "<br>";
            echo "$gender";
            ?>
        </div>
        <footer>
            Copyright 2022 @galihwahyusatrio._
        </footer>
</body>
</html>