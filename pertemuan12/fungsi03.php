<?php
//fungsi ini tanpa return value, & tanpa paramater
function luas_lingkaran($jari){
	return 3.14*$jari*$jari;
}

//pemanggilan fungsi
$r=17;
echo "Luas Lingkaran dengan jari-jari $r = ";
echo luas_lingkaran($r);
?>